<?php 

class Database {
	
	public function connect($config) {

		try{
			$conn = new PDO('mysql:host=localhost;dbname=' . $config['DB_NAME'] . ';', $config['DB_USERNAME'], $config['DB_PASSWORD']);
			$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			return $conn;

		} catch (Exception $e) {
			return false;
		}
	}

}





/*
This will be db class,
open conection
run queries
	select by id
	select by sql
	return array
	num rows
	get insert id
	select "sql" where "sql"?

close connection



will have seperate class for dealing with users and posts
those classes will extend from the db class
*/


//also insert queries?


?>