<?php
//constants & configs
defined('CORE_PATH') ? null : define('CORE_PATH', 'core/');
defined('CLASS_PATH') ? null : define('CLASS_PATH', CORE_PATH . 'classes/');
defined('PLUGIN_PATH') ? null : define('PLUGIN_PATH', CORE_PATH . 'plugins/');

date_default_timezone_set('UTC');

$config = array(
	'DB_USERNAME' => 'qrsharec_blog',
	'DB_PASSWORD' => 'holbrook1',
	'DB_NAME' => 'qrsharec_blog'
);

//include all classes 
include CLASS_PATH . 'Database.php';

//include functions
include CORE_PATH . 'functions.php';

//include nesecary plugins


//Test DB Connection
$DBH = new Database();
$conn = $DBH->connect($config);

if(!$conn) {
	die("Could not connect!");
}

//Get blog posts
//id, title, excerpt, date, author name, categories
$stmt = $conn->prepare('SELECT id_post, title, LEFT(content, 350) AS excerpt, date FROM tbl_posts');
$stmt->execute();
$posts = $stmt->fetchAll(PDO::FETCH_ASSOC);

//include views
include 'header.php';
include 'blog.php';
include 'footer.php';
?>




<?php 
/*
close db connection
die()
*/
?>