<?php

function convert_timestamp($timestamp, $format) {
	$exp = explode(" ", $timestamp);
	$date = explode("-", $exp[0]);
	$year = $date[0];
	$month = $date[1];
	$day = $date[2];
	
	$time = explode(":", $exp[1]);
	$hour = $time[0];
	$minute = $time[1];
	$second = $time[2];

	return date($format,mktime($hour, $minute, $second, $month, $day, $year));
}


?>