
			
			
			
			<!-- Content -->
			<div id="content">
			
				<!-- masthead -->
		        <div id="masthead">
					<span class="head">Blog</span><span class="subhead">this is a sub heading</span>
					<!-- <ul class="breadcrumbs">
						<li><a href="index.html">home</a></li>
						<li>/ blog</li>
					</ul> -->
				</div>
	        	<!-- ENDS masthead -->
	        	
	        	
	        	
	        	<!-- posts list -->
	        	<div id="posts-list">
	        	

	        		<?php
	        			foreach ($posts as $post) {
	        				$date = convert_timestamp($post['date'], "jS M, Y");
	        				echo '<article class="format-standard">';
	        				echo '<h1><a href="single.html" class="post-heading">'.$post['title'].'</a></h1>';
	        				echo '<div class="meta">';
	        				echo '<span class="entry-date">'.$date.' </span>';
	        				echo 'in <span class="categories"><a href="#">Category 1</a>, <a href="#">Category 2</a></span>';
	        				echo '</div>';
	        				echo '<div class="excerpt">' . $post['excerpt'] . '...</div>';
	        				echo '<a href="single.html" class="read-more">read more</a>';
	        				echo '</article>';
	        			}


	        		?>



					<!-- <article class="format-standard">
						
						<div class="feature-image">
							<a href="img/slides/01.jpg" data-rel="prettyPhoto"><img src="img/slides/01.jpg" alt="Alt text" /></a>
						</div>
						
						<h1><a href="single.html" class="post-heading">Lorem ipsum dolor amet</a></h1>
						<div class="meta">
							<span class="entry-date">23 Sep, 2011</span>
							in <span class="categories"><a href="#">Category 1</a>, <a href="#">Category 2</a></span>
						</div>
						
						
						<div class="excerpt">Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed.
						</div>
						
						<a href="single.html" class="read-more">read more</a>
						
					</article>
					
					
					<article class="format-standard">
						
						<div class="feature-image">
							<a href="img/slides/02.jpg" data-rel="prettyPhoto"><img src="img/slides/02.jpg" alt="Alt text" /></a>
						</div>
						
						<h1><a href="single.html" class="post-heading">Lorem ipsum dolor amet</a></h1>
						<div class="meta">
							<span class="entry-day">23</span>
							<span class="entry-year">sep 2011</span>
						</div>
						
						
						<div class="excerpt">Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed.
						</div>
						
						<a href="single.html" class="read-more">read more</a>
						
					</article>
					
					
					<article class="format-standard">
						
						<div class="feature-image">
							<a href="img/slides/03.jpg" data-rel="prettyPhoto"><img src="img/slides/03.jpg" alt="Alt text" /></a>
						</div>
						
						<h1><a href="single.html" class="post-heading">Lorem ipsum dolor amet</a></h1>
						<div class="meta">
							<span class="entry-day">23</span>
							<span class="entry-year">sep 2011</span>
						</div>
						
						
						<div class="excerpt">Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed.
						</div>
						
						<a href="single.html" class="read-more">read more</a>
						
					</article> -->
	        		
	        		
	        	</div>
	        	<!-- ENDS posts list -->
	        	
	        	
	        	<?php include('sidebar.php'); ?>