<?php
include_once 'header.php';
include_once 'sidebar.php';
?>

        <div class="span9">
          
          <div class="row-fluid">
            <div class="span12">
              <div class="title" style="text-align:center;">
                <h2>This is the super awesome blog post heading!</h2>
                <em>Tags: blah, blah. Categories: cat1, cat2. Date: 22 July 2012</em>
                <br><br>
              </div>  
              <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
              <p><a class="btn" href="#">View details &raquo;</a></p>
            </div><!--/span-->
          </div><!--/row-->
        </div><!--/span-->
      </div><!--/row-->

      <hr>
<?php include 'footer.php'; ?>